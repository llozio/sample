const gulp = require('gulp');
const rollup = require('rollup');
const pug = require('gulp-pug');
const sass = require('gulp-sass');
const sourcemaps = require('gulp-sourcemaps');
const connect = require('gulp-connect');


//+ 视图
let pugOptions = {
    filename: 'Jade',  // 如果是 'Jade'，inlclude 或 extends 默认为 Jade
    pretty: true
}
gulp.task('views', function compileViews () {
    return gulp.src('src/pc/components/p-demo/p-demo.pug')
        .pipe(pug(pugOptions))
        .pipe(gulp.dest('dist/pc/html'));
});
//- 视图


//+ 样式
gulp.task('styles', function compileStyles() {
    return gulp.src('src/pc/components/p-demo/p-demo.scss')
        .pipe(sourcemaps.init())
        .pipe(sass().on('error', sass.logError))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('dist/pc/css'));
});
//- 样式


//+ 脚本
gulp.task('scripts', async function compileScripts () {
    const bundle = await rollup.rollup({
        input: './src/pc/components/p-demo/p-demo.js'
    });

    await bundle.write({
        file: './dist/pc/js/p-demo.js',
        format: 'iife',
        name: 'pDemo',
        sourcemap: true
    });
});
//- 脚本


//+ 开发服务器
gulp.task('connect', function () {
    connect.server();
});
//- 开发服务器


gulp.task('default', ['views', 'styles', 'scripts']);
