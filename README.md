

使用 ESModule, 因为 AMD 测试源码比较麻烦。

测试源码是因为，不需要等待构建好项目。可以在单元测试失败后终止构建。

# 升级说明 #

### 0. 使用了 NodeJS v8.6.0 ###
如果使用了nvm, 下次打开shell对话，可能加载的还是上一个版本的node，可以把 8.6.0 设置成默认的版本： 

```
$ nvm alias default v8.6.0
```


### 1. Jade 升级到了 Pug ###
1. 所有的 *.jade 文件改成了 *.pug

Pug的新特性有: [Pug有哪些新特性?](https://pugjs.org/api/migration-v2.html)
1. a(href='before#{link}after') 改成了ES6的模板字符串 a(href=`before${link}after`) 
2. 删除 - 符号

Pros: 可能会提升构建速度
Cons: 更改为 ${} 可能要改动不少JSP代码

递归批量重命名： 
```
cd src

find . -name "*.jade" -exec bash -c 'mv "$1" "${1%.jade}".pug' - '{}' \;
```

### 2. 更新 SASS ### 
1. 添加 sourcemaps
2.  